import * as React from 'react';
import styles from './Toolbar.module.scss';
import {
    entryPageRoutes
} from 'routes'
import { Link, NavLink } from "react-router-dom";
import Logo from 'assets/imgs/logo.svg'
import { FormattedMessage } from "react-intl";

export interface BaseProps {
    readonly dumm?: boolean;
    readonly open?: boolean;
    readonly url: string;
    readonly roles: string[];

}

const ToolBarMenuComponent: React.FC<BaseProps> = props => {


    const {
        url,
        roles,
    } = props
    return (
        <div className={styles.container}>
            <div className={styles.links}>
                {entryPageRoutes(url, roles).map(route => (
                    <>
                        {route.title_type ? (
                            <FormattedMessage
                                id={route.title!}
                            />
                        ) : (
                                <NavLink
                                    activeClassName={styles.menu_item_active}
                                    to={route.to!}
                                    className={styles.menu_item}
                                >
                                    <span className={styles.menu_text}>
                                        <FormattedMessage
                                            id={route.title!}
                                        />
                                    </span>
                                </NavLink>
                            )}
                    </>
                ))}
            </div>
        </div>
    );
}

export {
    ToolBarMenuComponent as ToolBarMenu,
}
