import ListFraud from 'assets/imgs/user.svg'
import LabelIcon from 'assets/imgs/user.svg'
import FraudSearchIcon from 'assets/imgs/user.svg'
import SettingsIcon from 'assets/imgs/user.svg'
import ListIcon from 'assets/imgs/user.svg'
import MembersIcon from 'assets/imgs/user.svg'
import MessageIcon from 'assets/imgs/user.svg'
import {
    SettingsPage,
    AdminDashboardPage,
    AdministrationPage,
    GeneralPage,
} from 'pages'

import {
    parseRoles,
} from 'utilities'
import {
    ROLE_ADMINISTRATOR,
} from "constants/roles"
import { Component } from 'react'

interface Route {
    to?: string;
    title?: string;
    component?: any;
    roles?: string[];
    title_type?: boolean;
    exact?: boolean;
}

type Routes = (url: string, roles: string[]) => Route[]

const getRoutesPerRoles: Routes = (baseRoute = '/me', roles: string[]) => {
    const routes: Route[] = [
        {
            title_type: false,
            to: '/general',
            title: "routes.general",
            roles: parseRoles([
                ROLE_ADMINISTRATOR,
            ]),
            component: GeneralPage,
        }, /* General section */
        {
            exact: true,
            to: '/sudo',
            title: "routes.dashboard",
            component: AdminDashboardPage,
            roles: parseRoles([
                ROLE_ADMINISTRATOR,
            ])
        },
        {
            exact: true,
            to: '/administration',
            title: "routes.administration",
            component: AdministrationPage,
            roles: parseRoles([
            ])
        },/* Ending general section */
        {
            title_type: false,
            title: "routes.settings",
            roles: parseRoles([
                ROLE_ADMINISTRATOR,
            ]),
            component: SettingsPage,
        }, /* Ending settings section */
    ]
    const parsedRoutes = routes.map(route => ({ ...route, to: `${baseRoute}${route.to}` }))

    const filteredRoutesPerRole = parsedRoutes.filter(
        route => roles.some(
            role => route.roles!.includes(role.toLocaleLowerCase())
        )
    )

    return filteredRoutesPerRole
}

export {
    getRoutesPerRoles as entryPageRoutes
}