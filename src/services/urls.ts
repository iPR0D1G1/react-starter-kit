/*Urls for the application */
export const prefixer = 'localhost:8000/api/v1/';

export const authUrls = {
	//Auth URI
	SIGNINUSER: `${prefixer}`,
	LOGINUSER: `${prefixer}authentication/login`,
};

